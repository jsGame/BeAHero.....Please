//classe Mère qui servira créer les propriétés que les personnages auront en commun

import { Equipment } from "./equipment";
import { Job } from "./job";

export class Character {
  constructor(params) {
    this.name = params.name;
    this.hp = params.hp;
    this.equipment = new Equipment(params.equipment);
    this.job = new Job(params.job);

  }
}

